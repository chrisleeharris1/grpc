import { DirectSecp256k1HdWallet, Registry } from "@cosmjs/proto-signing";
import { defaultRegistryTypes, SigningStargateClient } from "@cosmjs/stargate";
//import { MsgXxx } from "./codec";
import { stringToPath } from "@cosmjs/crypto";

export const signAndSendTransaction = async () => {
	try {
		const myRegistry = new Registry([
  		...defaultRegistryTypes//,
  		//["/my.custom.MsgXxx", MsgXxx],
		]);
		
		const mnemonic =
			"lyrics feed stuff sing guard caught motion snap discover estate outdoor company guess mammal limit neck silly element element exotic orchard peasant zoo excuse";

			// Inside an async function...
		const signer = await DirectSecp256k1HdWallet.fromMnemonic(
  				mnemonic,
				//stringToPath("m/0'/1/2'/2/1000000000"),
				//"blub"  
				undefined,
  				"cosmos",
			);
		console.log("signer", signer)

		const client = await SigningStargateClient.connectWithSigner(
			"http://localhost:26657",
  			signer,
  		{
    		registry: myRegistry,
  			},
		);
	    console.log("client", client)

		const myAddress = "cosmos1a7arwva0qx5dfw4kjst3k70x4394g9lrpunqsz";
		const message = {
	  			//typeUrl: "/my.custom.MsgXxx",
				  typeUrl: "/cosmos.bank.v1beta1.MsgSend",
				  value: {
					fromAddress:"cosmos1a7arwva0qx5dfw4kjst3k70x4394g9lrpunqsz",
					toAddress:"cosmos1409pnt7ecqr6x326607607jz76twzpv9ttepxk",
					amount: [
						{
						  denom: "token",
						  amount: "10",
							},
						  ]	  
					}
			};
		const fee = {
	  		amount: [
			{
		  	denom: "token",
		  	amount: "10",
				},
  			],
	  		gas: "100500",
			};

		// Inside an async function...
		// This method uses the registry you provided
		console.log("signing and broadcasting")
		const response = await client.signAndBroadcast(myAddress, [message], fee);
		console.log("response", response)

		}

		catch(e){
			console.log("error", e)
  		}}

signAndSendTransaction()