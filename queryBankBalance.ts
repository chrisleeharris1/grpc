import { createRpc, QueryClient } from "@cosmjs/stargate";
import { Tendermint34Client } from "@cosmjs/tendermint-rpc";
console.log("imported1")
import { QueryClientImpl } from "./codec/cosmos/bank/v1beta1/query";
//import Query = require("./codec/cosmos/bank/v1beta1/query"); 

console.log("imported2")

export const queryClient = async () => {
	try {
// Inside an async function...
// The Tendermint client knows how to talk to the Tendermint RPC endpoint
const tendermintClient = await Tendermint34Client.connect("http://localhost:26657");
console.log(tendermintClient)


// The generic Stargate query client knows how to use the Tendermint client to submit unverified ABCI queries
const queryClient = new QueryClient(tendermintClient);
console.log(queryClient)


 // This helper function wraps the generic Stargate query client for use by the specific generated query client
const rpcClient = createRpc(queryClient);
console.log("rpc client", rpcClient)

// Here we instantiate a specific query client which will have the custom methods defined in the .proto file
const queryService = new QueryClientImpl(rpcClient);

//console.log("queryService", queryService)

// Now you can use this service to submit queries
const queryResult = await queryService.Balance({
  address: "cosmos1a7arwva0qx5dfw4kjst3k70x4394g9lrpunqsz",
  denom: "token"

}); 

	console.log("made it to end", queryResult)
	}
catch(e){
	console.log("error", e)
  }}

queryClient()

 