"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.Params = exports.ModuleAccount = exports.BaseAccount = exports.protobufPackage = void 0;
/* eslint-disable */
var long_1 = require("long");
var minimal_1 = require("protobufjs/minimal");
var any_1 = require("../../../google/protobuf/any");
exports.protobufPackage = "cosmos.auth.v1beta1";
var baseBaseAccount = {
    address: "",
    accountNumber: long_1["default"].UZERO,
    sequence: long_1["default"].UZERO
};
exports.BaseAccount = {
    encode: function (message, writer) {
        if (writer === void 0) { writer = minimal_1["default"].Writer.create(); }
        if (message.address !== "") {
            writer.uint32(10).string(message.address);
        }
        if (message.pubKey !== undefined) {
            any_1.Any.encode(message.pubKey, writer.uint32(18).fork()).ldelim();
        }
        if (!message.accountNumber.isZero()) {
            writer.uint32(24).uint64(message.accountNumber);
        }
        if (!message.sequence.isZero()) {
            writer.uint32(32).uint64(message.sequence);
        }
        return writer;
    },
    decode: function (input, length) {
        var reader = input instanceof minimal_1["default"].Reader ? input : new minimal_1["default"].Reader(input);
        var end = length === undefined ? reader.len : reader.pos + length;
        var message = __assign({}, baseBaseAccount);
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.address = reader.string();
                    break;
                case 2:
                    message.pubKey = any_1.Any.decode(reader, reader.uint32());
                    break;
                case 3:
                    message.accountNumber = reader.uint64();
                    break;
                case 4:
                    message.sequence = reader.uint64();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON: function (object) {
        var message = __assign({}, baseBaseAccount);
        if (object.address !== undefined && object.address !== null) {
            message.address = String(object.address);
        }
        else {
            message.address = "";
        }
        if (object.pubKey !== undefined && object.pubKey !== null) {
            message.pubKey = any_1.Any.fromJSON(object.pubKey);
        }
        else {
            message.pubKey = undefined;
        }
        if (object.accountNumber !== undefined && object.accountNumber !== null) {
            message.accountNumber = long_1["default"].fromString(object.accountNumber);
        }
        else {
            message.accountNumber = long_1["default"].UZERO;
        }
        if (object.sequence !== undefined && object.sequence !== null) {
            message.sequence = long_1["default"].fromString(object.sequence);
        }
        else {
            message.sequence = long_1["default"].UZERO;
        }
        return message;
    },
    toJSON: function (message) {
        var obj = {};
        message.address !== undefined && (obj.address = message.address);
        message.pubKey !== undefined &&
            (obj.pubKey = message.pubKey ? any_1.Any.toJSON(message.pubKey) : undefined);
        message.accountNumber !== undefined &&
            (obj.accountNumber = (message.accountNumber || long_1["default"].UZERO).toString());
        message.sequence !== undefined &&
            (obj.sequence = (message.sequence || long_1["default"].UZERO).toString());
        return obj;
    },
    fromPartial: function (object) {
        var message = __assign({}, baseBaseAccount);
        if (object.address !== undefined && object.address !== null) {
            message.address = object.address;
        }
        else {
            message.address = "";
        }
        if (object.pubKey !== undefined && object.pubKey !== null) {
            message.pubKey = any_1.Any.fromPartial(object.pubKey);
        }
        else {
            message.pubKey = undefined;
        }
        if (object.accountNumber !== undefined && object.accountNumber !== null) {
            message.accountNumber = object.accountNumber;
        }
        else {
            message.accountNumber = long_1["default"].UZERO;
        }
        if (object.sequence !== undefined && object.sequence !== null) {
            message.sequence = object.sequence;
        }
        else {
            message.sequence = long_1["default"].UZERO;
        }
        return message;
    }
};
var baseModuleAccount = { name: "", permissions: "" };
exports.ModuleAccount = {
    encode: function (message, writer) {
        if (writer === void 0) { writer = minimal_1["default"].Writer.create(); }
        if (message.baseAccount !== undefined) {
            exports.BaseAccount.encode(message.baseAccount, writer.uint32(10).fork()).ldelim();
        }
        if (message.name !== "") {
            writer.uint32(18).string(message.name);
        }
        for (var _i = 0, _a = message.permissions; _i < _a.length; _i++) {
            var v = _a[_i];
            writer.uint32(26).string(v);
        }
        return writer;
    },
    decode: function (input, length) {
        var reader = input instanceof minimal_1["default"].Reader ? input : new minimal_1["default"].Reader(input);
        var end = length === undefined ? reader.len : reader.pos + length;
        var message = __assign({}, baseModuleAccount);
        message.permissions = [];
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.baseAccount = exports.BaseAccount.decode(reader, reader.uint32());
                    break;
                case 2:
                    message.name = reader.string();
                    break;
                case 3:
                    message.permissions.push(reader.string());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON: function (object) {
        var message = __assign({}, baseModuleAccount);
        message.permissions = [];
        if (object.baseAccount !== undefined && object.baseAccount !== null) {
            message.baseAccount = exports.BaseAccount.fromJSON(object.baseAccount);
        }
        else {
            message.baseAccount = undefined;
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = String(object.name);
        }
        else {
            message.name = "";
        }
        if (object.permissions !== undefined && object.permissions !== null) {
            for (var _i = 0, _a = object.permissions; _i < _a.length; _i++) {
                var e = _a[_i];
                message.permissions.push(String(e));
            }
        }
        return message;
    },
    toJSON: function (message) {
        var obj = {};
        message.baseAccount !== undefined &&
            (obj.baseAccount = message.baseAccount
                ? exports.BaseAccount.toJSON(message.baseAccount)
                : undefined);
        message.name !== undefined && (obj.name = message.name);
        if (message.permissions) {
            obj.permissions = message.permissions.map(function (e) { return e; });
        }
        else {
            obj.permissions = [];
        }
        return obj;
    },
    fromPartial: function (object) {
        var message = __assign({}, baseModuleAccount);
        message.permissions = [];
        if (object.baseAccount !== undefined && object.baseAccount !== null) {
            message.baseAccount = exports.BaseAccount.fromPartial(object.baseAccount);
        }
        else {
            message.baseAccount = undefined;
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = object.name;
        }
        else {
            message.name = "";
        }
        if (object.permissions !== undefined && object.permissions !== null) {
            for (var _i = 0, _a = object.permissions; _i < _a.length; _i++) {
                var e = _a[_i];
                message.permissions.push(e);
            }
        }
        return message;
    }
};
var baseParams = {
    maxMemoCharacters: long_1["default"].UZERO,
    txSigLimit: long_1["default"].UZERO,
    txSizeCostPerByte: long_1["default"].UZERO,
    sigVerifyCostEd25519: long_1["default"].UZERO,
    sigVerifyCostSecp256k1: long_1["default"].UZERO
};
exports.Params = {
    encode: function (message, writer) {
        if (writer === void 0) { writer = minimal_1["default"].Writer.create(); }
        if (!message.maxMemoCharacters.isZero()) {
            writer.uint32(8).uint64(message.maxMemoCharacters);
        }
        if (!message.txSigLimit.isZero()) {
            writer.uint32(16).uint64(message.txSigLimit);
        }
        if (!message.txSizeCostPerByte.isZero()) {
            writer.uint32(24).uint64(message.txSizeCostPerByte);
        }
        if (!message.sigVerifyCostEd25519.isZero()) {
            writer.uint32(32).uint64(message.sigVerifyCostEd25519);
        }
        if (!message.sigVerifyCostSecp256k1.isZero()) {
            writer.uint32(40).uint64(message.sigVerifyCostSecp256k1);
        }
        return writer;
    },
    decode: function (input, length) {
        var reader = input instanceof minimal_1["default"].Reader ? input : new minimal_1["default"].Reader(input);
        var end = length === undefined ? reader.len : reader.pos + length;
        var message = __assign({}, baseParams);
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.maxMemoCharacters = reader.uint64();
                    break;
                case 2:
                    message.txSigLimit = reader.uint64();
                    break;
                case 3:
                    message.txSizeCostPerByte = reader.uint64();
                    break;
                case 4:
                    message.sigVerifyCostEd25519 = reader.uint64();
                    break;
                case 5:
                    message.sigVerifyCostSecp256k1 = reader.uint64();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON: function (object) {
        var message = __assign({}, baseParams);
        if (object.maxMemoCharacters !== undefined &&
            object.maxMemoCharacters !== null) {
            message.maxMemoCharacters = long_1["default"].fromString(object.maxMemoCharacters);
        }
        else {
            message.maxMemoCharacters = long_1["default"].UZERO;
        }
        if (object.txSigLimit !== undefined && object.txSigLimit !== null) {
            message.txSigLimit = long_1["default"].fromString(object.txSigLimit);
        }
        else {
            message.txSigLimit = long_1["default"].UZERO;
        }
        if (object.txSizeCostPerByte !== undefined &&
            object.txSizeCostPerByte !== null) {
            message.txSizeCostPerByte = long_1["default"].fromString(object.txSizeCostPerByte);
        }
        else {
            message.txSizeCostPerByte = long_1["default"].UZERO;
        }
        if (object.sigVerifyCostEd25519 !== undefined &&
            object.sigVerifyCostEd25519 !== null) {
            message.sigVerifyCostEd25519 = long_1["default"].fromString(object.sigVerifyCostEd25519);
        }
        else {
            message.sigVerifyCostEd25519 = long_1["default"].UZERO;
        }
        if (object.sigVerifyCostSecp256k1 !== undefined &&
            object.sigVerifyCostSecp256k1 !== null) {
            message.sigVerifyCostSecp256k1 = long_1["default"].fromString(object.sigVerifyCostSecp256k1);
        }
        else {
            message.sigVerifyCostSecp256k1 = long_1["default"].UZERO;
        }
        return message;
    },
    toJSON: function (message) {
        var obj = {};
        message.maxMemoCharacters !== undefined &&
            (obj.maxMemoCharacters = (message.maxMemoCharacters || long_1["default"].UZERO).toString());
        message.txSigLimit !== undefined &&
            (obj.txSigLimit = (message.txSigLimit || long_1["default"].UZERO).toString());
        message.txSizeCostPerByte !== undefined &&
            (obj.txSizeCostPerByte = (message.txSizeCostPerByte || long_1["default"].UZERO).toString());
        message.sigVerifyCostEd25519 !== undefined &&
            (obj.sigVerifyCostEd25519 = (message.sigVerifyCostEd25519 || long_1["default"].UZERO).toString());
        message.sigVerifyCostSecp256k1 !== undefined &&
            (obj.sigVerifyCostSecp256k1 = (message.sigVerifyCostSecp256k1 || long_1["default"].UZERO).toString());
        return obj;
    },
    fromPartial: function (object) {
        var message = __assign({}, baseParams);
        if (object.maxMemoCharacters !== undefined &&
            object.maxMemoCharacters !== null) {
            message.maxMemoCharacters = object.maxMemoCharacters;
        }
        else {
            message.maxMemoCharacters = long_1["default"].UZERO;
        }
        if (object.txSigLimit !== undefined && object.txSigLimit !== null) {
            message.txSigLimit = object.txSigLimit;
        }
        else {
            message.txSigLimit = long_1["default"].UZERO;
        }
        if (object.txSizeCostPerByte !== undefined &&
            object.txSizeCostPerByte !== null) {
            message.txSizeCostPerByte = object.txSizeCostPerByte;
        }
        else {
            message.txSizeCostPerByte = long_1["default"].UZERO;
        }
        if (object.sigVerifyCostEd25519 !== undefined &&
            object.sigVerifyCostEd25519 !== null) {
            message.sigVerifyCostEd25519 = object.sigVerifyCostEd25519;
        }
        else {
            message.sigVerifyCostEd25519 = long_1["default"].UZERO;
        }
        if (object.sigVerifyCostSecp256k1 !== undefined &&
            object.sigVerifyCostSecp256k1 !== null) {
            message.sigVerifyCostSecp256k1 = object.sigVerifyCostSecp256k1;
        }
        else {
            message.sigVerifyCostSecp256k1 = long_1["default"].UZERO;
        }
        return message;
    }
};
if (minimal_1["default"].util.Long !== long_1["default"]) {
    minimal_1["default"].util.Long = long_1["default"];
    minimal_1["default"].configure();
}
